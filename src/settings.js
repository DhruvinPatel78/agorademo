import { createClient, createMicrophoneAndCameraTracks } from "agora-rtc-react";

const appId = "a547979e1c3d4f56a845dbb4d8e90d79";
const token = null;

export const config = { mode: "rtc", codec: "vp8", appId: appId, token: token };
export const useClient = createClient(config);
export const useMicrophoneAndCameraTracks = createMicrophoneAndCameraTracks();
export const channelName = "beet";
